# aware_test
* For the JSON, if it is too large, we should only read record one-by-one.

* For the deviceID search:
Keep reading until found the desired ID, and return the device record.

* For the paginated search:
Based on the paginate number, we could determine the index of our search records (paginate * RECORDSIZE). And we got the FROM-TO couple.

To improve performance, we ignore the record that is not belongs to the FROM-TO. Also, if we already at the TO index, break right away for efficient.

* For the requirement for "The end results should be a database containing the devices, presenting the latest version found in the JSON", I used the md5hash to check whether the **devices.json** has been updated. If it is updated, then all the caching for the current request will be overwrited. If not, then we can reuse the cached file. ( For further development, another update idea such as caching based on record index can be made, so that if one record is added into the **devices.json**, we still can reuse the cached from previous record index)

# The structure of the project is as follows:

- main.go: The main file for running

- cmd/root.go: For simplicity, I used this one to run a server

- cmd/service.go: This file is a service that used to process and return the data to the root.go

- cmd/util.go: Utilities function

- database/: The folder to store the JSON file 

- cache/: The folder to store the requested cached result 

- tests/: Contains the test-cases for our funtions ( not all )

# The following tasks need to be done to improve:
- [x] Make a helper for the API function ( Stated in the @TODO )
- [ ] Validate the input of each variables
- [x] Update the cache & md5sum check to improve the request 
- [x] CI/CD and unit test 


# USAGE
## To run API server: 
`go run main.go` 
The REST API can be used as follows: 
* To collect the devices with paginate: `http://localhost:10000/devices/_page_` 
* To collect the device ID `http://localhost:10000/device/_id_`
* To collect the device by status: `http://localhost:10000/devices/status/_status_value_/_page_`
* To collect the device by type:  `http://localhost:10000/devices/type/_type_/_page_`

Where the _type_ is the type of devices, and _page_ is the paginate value

## To run Service function:
`go run main.go service <Type> <Args>`
- Type can be: deviceID (argument: 1 - For ID), status (arguments: 2 includes: status and page number), type (arguments: 2 includes type and page number), devices (argument only need page number)

# HOW TO TEST
* The code will be test everytime I have a commit to gitlab. To extend this project, we only need to add more test-cases.
* You can run function tests by executing
`go test gitlab.com/cntu.cnsl/aware_test/tests/`
