package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

// We use normal Unmarshal here since we know that the cache data
// is limited based on the RECORDSIZE
func GetFromDatabase(FileName string) (Devices, error) {
	// Open our jsonFile
	fmt.Println("Collect from database")
	StoredFile := string("cache") + string(os.PathSeparator) + FileName
	var devices Devices
	devices.Ready = false

	jsonFile, err := os.Open(StoredFile)
	// if we os.Open returns an error then handle it
	if err != nil {
		return devices, err
	}
	// defer the closing of our jsonFile so that we can parse it later on
	json.NewDecoder(jsonFile).Decode(&devices)
	devices.Ready = true

	return devices, nil
}

func WriteToDatabase(fileName string, devices Devices) (bool, error) {
	fmt.Println("Writing to database")
	file, _ := json.MarshalIndent(devices, "", " ")
	StoredFile := string("cache") + string(os.PathSeparator) + fileName
	fmt.Printf("%v", StoredFile)
	_ = ioutil.WriteFile(StoredFile, file, 0644)
	return true, nil
}
