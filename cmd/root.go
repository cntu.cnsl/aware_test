/*
Copyright © 2021 TuCN

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/spf13/cobra"

	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "aware_test",
	Short: "Test application for Awair",
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Rest API Server - Aware Test")
		handleRequests()
	},
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")
	fmt.Println("Endpoint Hit: homePage")
}

func handleRequests() {
	// creates a new instance of a mux router
	if _, err := os.Stat("cache"); os.IsNotExist(err) {
		err := os.Mkdir("cache", 0755)
		if err != nil {
			log.Fatal("Cannot create cache folder")
		}
	}
	MyRouter := mux.NewRouter().StrictSlash(true)
	MyRouter.HandleFunc("/", homePage)
	MyRouter.HandleFunc("/device/{id}", ReturnDeviceID)
	MyRouter.HandleFunc("/devices/type/{type}/{page}", ReturnDeviceByTypes)
	MyRouter.HandleFunc("/devices/status/{status}/{page}", ReturnStatus)
	log.Fatal(http.ListenAndServe(":10000", MyRouter))
}

func ReturnStatus(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	StatusType := vars["status"]
	Page := vars["page"]
	PageInt, err := strconv.Atoi(Page)
	// If page is given non-integer
	// Consider they try to get Page = 0
	if err != nil {
		PageInt = 0
	}
	var PageFrom, PageTo = 0, RECORDSIZE

	if PageInt > 0 {
		PageTo = PageInt * RECORDSIZE
		PageFrom = (int(PageInt) - 1) * RECORDSIZE
	}
	// Check if the devices.json is updated or not
	// if it is not updated, we can use the cache if exists
	HashUpdated, err := CheckSum(DATABASEPATH)
	if err != nil {
		log.Fatalf("Something wrong while doing checksum")
		panic(err)
	}
	devices, err := ReturnStatusService(StatusType, PageTo, PageFrom, HashUpdated)
	if err != nil {
		panic(err)
	}
	json.NewEncoder(w).Encode(devices)
}

func ReturnDevices(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	// @TODO: Validate input
	Page := vars["page"]
	PageInt, err := strconv.Atoi(Page)
	if err != nil {
		PageInt = 0
	}
	// @TODO: check if Page is integer
	var PageFrom, PageTo = 0, RECORDSIZE

	if PageInt > 0 {
		PageTo = PageInt * RECORDSIZE
		PageFrom = (int(PageInt) - 1) * RECORDSIZE
	}
	// Check if the devices.json is updated or not
	// if it is not updated, we can use the cache if exists
	HashUpdated, err := CheckSum(DATABASEPATH)
	if err != nil {
		log.Fatalf("Something wrong while doing checksum")
		panic(err)
	}
	devices, err := ReturnDevicesService(PageTo, PageFrom, HashUpdated)
	if err != nil {
		panic(err)
	}
	json.NewEncoder(w).Encode(devices)
}

func ReturnDeviceID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["id"]
	device, ok := ReturnDeviceIDService(key)
	if !ok {
		log.Printf("Device ID not found")
	}
	json.NewEncoder(w).Encode(device)

}

func ReturnDeviceByTypes(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	InputType := vars["type"]
	Page := vars["page"]
	PageInt, err := strconv.Atoi(Page)
	if err != nil {
		PageInt = 0
	}
	var PageFrom, PageTo = 0, RECORDSIZE

	if PageInt > 1 {
		PageTo = PageInt * RECORDSIZE
		PageFrom = (int(PageInt) - 1) * RECORDSIZE
	}
	// Check if the devices.json is updated or not
	// if it is not updated, we can use the cache if exists
	HashUpdated, err := CheckSum(DATABASEPATH)
	if err != nil {
		// This is the problem from server side, I consider it as fatal
		// so that we can detect soon
		log.Fatalf("Something wrong while doing checksum")
		panic(err)
	}
	devices, err := ReturnDeviceByTypesService(InputType, PageTo, PageFrom, HashUpdated)
	if err != nil {
		panic(err)
	}
	json.NewEncoder(w).Encode(devices)
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.aware_test.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".aware_test" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".aware_test")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}
