package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(serviceCmd)
}

var serviceCmd = &cobra.Command{
	Use:   "service",
	Short: "Service that used to collect the data",
	Run: func(cmd *cobra.Command, args []string) {
		Cli(args)
	},
}

/*
Command-line function for collecting the devices information.

This function expected act as a middle-point between HTTP server
and database
*/
func Cli(args []string) {
	if len(args) < 1 {
		fmt.Println("More input needed")
		os.Exit(1)
	}
	CheckSum(DATABASEPATH)
	if _, err := os.Stat("cache"); os.IsNotExist(err) {
		err := os.Mkdir("cache", 0755)
		if err != nil {
			log.Fatal("Cannot create cache folder")
		}
	}
	switch args[0] {
	case "deviceID":
		if len(args) < 2 {
			fmt.Println("More input needed")
			os.Exit(1)
		}
		deviceId := args[1]
		output, ok := ReturnDeviceIDService(deviceId)
		if !ok {
			log.Printf("Device ID not found")
		}
		fmt.Printf("The request value: %v", output)
	case "status":
		if len(args) < 3 {
			fmt.Println("More input needed")
			os.Exit(1)
		}

		Status := args[1]
		Page := args[2]
		PageInt, err := strconv.Atoi(Page)
		// Raise error if page number given
		if err != nil {
			fmt.Println("Input page error")
			os.Exit(1)
		}
		// @TODO: check if Page is integer
		var PageFrom, PageTo = 0, RECORDSIZE
		if PageInt > 0 {
			PageTo = (PageInt + 1) * RECORDSIZE
			PageFrom = PageInt * RECORDSIZE
		}
		fmt.Printf("PageInt: %v, PageFrom: %v, PageTo: %v", PageInt, PageFrom, PageTo)

		output, err := ReturnStatusService(Status, PageTo, PageFrom, true)
		if err != nil {
			os.Exit(1)
		}
		fmt.Printf("The request value: %v", output)
	// Request for devices type
	case "type":
		if len(args) < 3 {
			fmt.Println("More input needed")
			os.Exit(1)
		}

		InputType := args[1]
		Page := args[2]
		PageInt, err := strconv.Atoi(Page)
		// Raise error if page number given
		if err != nil {
			os.Exit(1)
		}
		// @TODO: check if Page is integer
		var PageFrom, PageTo = 0, RECORDSIZE
		if PageInt > 0 {
			PageTo = PageInt * RECORDSIZE
			PageFrom = (int(PageInt) - 1) * RECORDSIZE
		}
		output, err := ReturnDeviceByTypesService(InputType, PageTo, PageFrom, true)
		if err != nil {
			os.Exit(1)
		}
		fmt.Printf("The request value: %v", output)
	case "devices":
		if len(args) < 2 {
			fmt.Println("More input needed")
			os.Exit(1)
		}

		Page := args[1]
		PageInt, err := strconv.Atoi(Page)
		// Raise error if page number given
		if err != nil {
			panic(err)
		}
		// @TODO: check if Page is integer
		var PageFrom, PageTo = 0, RECORDSIZE
		if PageInt > 0 {
			PageTo = (PageInt + 1) * RECORDSIZE
			PageFrom = PageInt * RECORDSIZE
		}
		Output, err := ReturnDevicesService(PageTo, PageFrom, true)
		if err != nil {
			os.Exit(1)
		}
		fmt.Printf("The request value: %v", Output)
	default:
		fmt.Println("Please specify the correct input")
		os.Exit(1)
	}
}

func (devices *Devices) addItem(device Device) []Device {
	devices.Items = append(devices.Items, device)
	return devices.Items
}

func ReturnStatusService(StatusType string, PageTo int, PageFrom int, CheckSum bool) (Devices, error) {
	// Check if there is a cached data
	CacheFile := "status-" + string(StatusType) + "-f" + strconv.Itoa(PageFrom) + "-t" + strconv.Itoa(PageTo)
	if CheckSum {
		devices, err := GetFromDatabase(CacheFile)
		if err == nil && devices.Ready {
			return devices, nil
		}
	}
	// Open our jsonFile
	jsonFile, _ := os.Open(DATABASEPATH)
	byteValue, _ := ioutil.ReadAll(jsonFile)
	// Unmarshal using a generic interface
	var f interface{}
	must(json.Unmarshal(byteValue, &f))

	// JSON object parses into a map with string keys
	itemsMap := f.(map[string]interface{})
	var devices Devices
	// Loop through the Items; we're not interested in the key, just the values
	var count = 0
	for _, v := range itemsMap {
		// Use type assertions to ensure that the value's a JSON object
		switch jsonObj := v.(type) {
		// The value is an Item, represented as a generic interface
		case interface{}:
			if count < PageFrom {
				count += 1
				continue
			}
			if count > PageTo {
				break
			}
			var device Device
			// Access the values in the JSON object and place them in an Item
			device, ok := getDeviceFromMap(jsonObj)
			if !ok {
				// Go to next record
				continue
			}
			if device.Status == StatusType && count >= PageFrom && count <= PageTo {
				fmt.Printf("Device matched: %v \n", device)
				devices.addItem(device)
				count += 1
			}

		}
	}
	WriteToDatabase(CacheFile, devices)
	return devices, nil
}

func ReturnDevicesService(PageTo int, PageFrom int, CheckSum bool) (Devices, error) {
	// Open our jsonFile
	CacheFile := string("devices") + "-f" + strconv.Itoa(PageFrom) + "-t" + strconv.Itoa(PageTo)
	if CheckSum {
		devices, err := GetFromDatabase(CacheFile)
		if err == nil && devices.Ready {
			return devices, nil
		}
	}
	JsonFile, _ := os.Open(DATABASEPATH)
	ByteValue, _ := ioutil.ReadAll(JsonFile)
	// Unmarshal using a generic interface
	var f interface{}
	must(json.Unmarshal(ByteValue, &f))

	// JSON object parses into a map with string keys
	itemsMap := f.(map[string]interface{})
	var devices Devices

	// Loop through the Items; we're not interested in the key, just the values
	var count = 0
	for _, v := range itemsMap {
		// Use type assertions to ensure that the value's a JSON object
		switch JsonObj := v.(type) {
		// The value is an Item, represented as a generic interface
		case interface{}:
			if count < PageFrom {
				count += 1
				continue
			}
			if count > PageTo {
				break
			}
			device, ok := getDeviceFromMap(JsonObj)
			if !ok {
				// Go to next record
				continue
			}
			if count >= PageFrom && count <= PageTo {
				fmt.Printf("Device matched: %v \n", device)
				devices.addItem(device)
				count += 1
			}

		}
	}
	WriteToDatabase(CacheFile, devices)
	return devices, nil
}

// This function is used to return the device by its types
func ReturnDeviceByTypesService(InputType string, PageTo int, PageFrom int, CheckSum bool) (Devices, error) {
	// Consider each page is 50 records
	CacheFile := "type-" + string(InputType) + "-f" + strconv.Itoa(PageFrom) + "-t" + strconv.Itoa(PageTo)
	if CheckSum {
		devices, err := GetFromDatabase(CacheFile)
		if err == nil && devices.Ready {
			return devices, nil
		}
	}

	// Open our jsonFile
	JsonFile, _ := os.Open(DATABASEPATH)
	ByteValue, _ := ioutil.ReadAll(JsonFile)
	// Unmarshal using a generic interface
	var f interface{}
	must(json.Unmarshal(ByteValue, &f))

	// JSON object parses into a map with string keys
	itemsMap := f.(map[string]interface{})
	var devices Devices

	// Loop through the Items; we're not interested in the key, just the values
	var count = 0
	for _, v := range itemsMap {
		// Use type assertions to ensure that the value's a JSON object
		switch JsonObj := v.(type) {
		// The value is an Item, represented as a generic interface
		case interface{}:
			if count < PageFrom || count > PageTo {
				break
			}
			// This part should convert into a helper function
			var device Device
			// Access the values in the JSON object and place them in an Item
			device, ok := getDeviceFromMap(JsonObj)
			if !ok {
				// Go to next record
				continue
			}
			if device.Type == InputType && count >= PageFrom && count <= PageTo {
				log.Printf("Device matched: %v \n", device)
				devices.addItem(device)
				count += 1
			}

		}
	}
	WriteToDatabase(CacheFile, devices)
	return devices, nil
}

// @TODO: Create a common file to ItemMap function
// I dont' think this one need cached
// We don't want to store 1million cache file with one line
func ReturnDeviceIDService(key string) (Device, bool) {
	// Open our jsonFile
	JsonFile, _ := os.Open(DATABASEPATH)
	ByteValue, _ := ioutil.ReadAll(JsonFile)
	// Unmarshal using a generic interface
	var f interface{}
	must(json.Unmarshal(ByteValue, &f))

	// JSON object parses into a map with string keys
	itemsMap := f.(map[string]interface{})
	var device Device

	// Loop through the Items; we're not interested in the key, just the values
	for k, v := range itemsMap {
		// Use type assertions to ensure that the value's a JSON object
		if key != k {
			continue
		}
		switch JsonObj := v.(type) {
		// The value is an Item, represented as a generic interface
		case interface{}:
			// Access the values in the JSON object and place them in an Item
			device, ok := getDeviceFromMap(JsonObj)
			if ok {
				// return the record
				return device, true
			}
		}
	}
	return device, false
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}

// This function is a helper function to get the device information
// from JSON object
// @not expose to outside
func getDeviceFromMap(jsonObj interface{}) (Device, bool) {
	// This part should convert into a helper function
	var device Device
	// Access the values in the JSON object and place them in an Item
	for deviceKey, deviceValue := range jsonObj.(map[string]interface{}) {
		switch deviceKey {
		case "id":
			switch deviceValue := deviceValue.(type) {
			case string:
				device.Id = deviceValue
			default:
				log.Println("Incorrect type for", deviceKey)
			}
		case "type":
			switch deviceValue := deviceValue.(type) {
			case string:
				device.Type = string(deviceValue)
			default:
				log.Println("Incorrect type for", deviceKey)
			}
		case "coordinates":
			switch deviceValue := deviceValue.(type) {
			case []interface{}:
				device.Coordinates = []interface{}(deviceValue)
			default:
				log.Println("Incorrect type for", deviceKey)
			}
		case "status":
			switch deviceValue := deviceValue.(type) {
			case string:
				device.Status = string(deviceValue)
			default:
				log.Println("Incorrect type for", deviceKey)
			}
		case "timezone":
			switch deviceValue := deviceValue.(type) {
			case string:
				device.TimeZone = string(deviceValue)
			default:
				log.Println("Incorrect type for", deviceKey)
			}

		default:
			log.Println("Unknown key for Item found in JSON")
		}

	}
	if device.Id == "" {
		return device, false
	}
	return device, true
}
