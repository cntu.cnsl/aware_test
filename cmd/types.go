package cmd

type Device struct {
	Id          string        `json:"id"`
	Type        string        `json:"type"`
	Coordinates []interface{} `json:"coordinates"`
	Status      string        `json:"status"`
	TimeZone    string        `json:"timezone"`
}

var RECORDSIZE = 50

type Devices struct {
	Items []Device
	Ready bool
}

type Database struct {
	Status []Devices
	Types  []Devices
	All    []Devices
}
