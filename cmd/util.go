package cmd

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
)

var HASHEDFILE = "devices.md5"
var DATABASEPATH = "database/devices.json"

// This function used for testing
func SetDatabase(path string) {
	DATABASEPATH = path
}

// Use for checksum the devices.json
func md5hash(filePath string) (string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		return "", err
	}
	FileHashed, err := hex.EncodeToString(hash.Sum(nil)), nil
	return FileHashed, nil
}

// Get existing checksum data
// If file not exist, we consider this
// as the first time we execute the problem
func CheckSum(DatabaseLocation string) (bool, error) {
	ChechHashLocation := string("cache") + string(os.PathSeparator) + HASHEDFILE
	StoredHash, err := ioutil.ReadFile(ChechHashLocation)
	var NewHash = false
	if err != nil {
		log.Printf("Unable to read file: %v", err)
		NewHash = true
	}
	// Calculate hash for file
	HashedValue, err := md5hash(DatabaseLocation)
	if err != nil {
		log.Printf("Error when hashing: %v", err)
		return false, err
	}
	if NewHash {
		// Write new hash file
		_ = ioutil.WriteFile(ChechHashLocation, []byte(HashedValue), 0644)
		return false, nil
	}
	if string(HashedValue) == string(StoredHash) {
		return true, nil
	}
	fmt.Printf("Hashed Value: %v", HashedValue)
	return false, nil
}
