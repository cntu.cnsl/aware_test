package tests

import (
	"testing"

	cmd "gitlab.com/cntu.cnsl/aware_test/cmd"
)

func TestReturnDeviceID(t *testing.T) {
	cmd.SetDatabase("test.json")
	_, ok := cmd.ReturnDeviceIDService("c44dt2ecie6h7m4li3cg")
	if !ok {
		t.Errorf("ReturnDeviceIDService() error = %v, wantErr %v",
			ok, true)
		return
	}

	_, ok = cmd.ReturnDeviceIDService("afasfdasda")
	if ok {
		t.Errorf("ReturnDeviceIDService() should not return OK")
		return
	}

	_, ok = cmd.ReturnDeviceIDService("c44dt2ecie6h7m4li3d0")
	if !ok {
		t.Errorf("ReturnDeviceIDService() error = %v, wantErr %v",
			ok, true)
		return
	}
}

func TestReturnStatusService(t *testing.T) {
	cmd.SetDatabase("test.json")
	_, err := cmd.ReturnStatusService("connected", 50, 0, false)
	if err != nil {
		t.Errorf("ReturnStatusService() error = %v, wantErr %v",
			err, nil)
		return
	}

	_, err = cmd.ReturnStatusService("disconnected", 100, 50, false)
	if err != nil {
		t.Errorf("ReturnStatusService() error = %v, wantErr %v",
			err, nil)
		return
	}
}

func TestReturnDevicesService(t *testing.T) {
	cmd.SetDatabase("test.json")
	_, err := cmd.ReturnDevicesService(50, 0, false)
	if err != nil {
		t.Errorf("ReturnStatusService() error = %v, wantErr %v",
			err, nil)
		return
	}

	_, err = cmd.ReturnDevicesService(100, 50, false)
	if err != nil {
		t.Errorf("ReturnStatusService() error = %v, wantErr %v",
			err, nil)
		return
	}
}

func TestReturnDeviceByTypesService(t *testing.T) {
	cmd.SetDatabase("test.json")
	_, err := cmd.ReturnDeviceByTypesService("awair-glow", 50, 0, false)
	if err != nil {
		t.Errorf("ReturnStatusService() error = %v, wantErr %v",
			err, nil)
		return
	}

	_, err = cmd.ReturnDeviceByTypesService("nono", 100, 50, false)
	if err != nil {
		t.Errorf("ReturnStatusService() should raise error ")
		return
	}
}
