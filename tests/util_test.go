package tests

import (
	"testing"

	cmd "gitlab.com/cntu.cnsl/aware_test/cmd"
)

func TestCheckSum(t *testing.T) {
	/* structure of input arguments
	 	type args struct {
			StatusType string
			PageTo     int
			PageFrom   int
			checksum   bool
		}
	*/

	_, err := cmd.CheckSum("test.json")
	if err != nil {
		t.Errorf("CheckSum() error = %v, wantErr %v",
			err, nil)
		return
	}

	_, err = cmd.CheckSum("not_exist.json")
	if err == nil {
		t.Errorf("CheckSum() error (file not exists)")
		return
	}
}
